const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const fs = require('fs');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const TerserJSPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');


function generateHtmlPlugins(templateDir) {
    const templateFiles = fs.readdirSync(path.resolve(__dirname, templateDir));
    return templateFiles.filter(item => fs.lstatSync(templateDir + '/' + item).isFile()).map(item => {
        const parts = item.split('.');
        const name = parts[0];
        const extension = parts[1];
        return new HtmlWebpackPlugin({
            filename: `prod/${name}.html`,
            template: path.resolve(__dirname, `${templateDir}/${name}.${extension}`),
            inject: false,
        })
    })
}
const htmlPlugins = generateHtmlPlugins('src');

module.exports = {
    optimization: {
      minimizer: [new TerserJSPlugin({}), new OptimizeCSSAssetsPlugin({})],
    },
    mode: 'production',
    entry: [
        './src/js/app.js',
        './src/scss/app.scss'
    ],
    output: {
        filename: './prod/js/app.js'
    },
    module: {
        rules: [{
            test: /\.js$/,
            include: path.resolve(__dirname, 'src/js'),
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ['@babel/preset-env']
                }
            }
        }, {
            test: /\.(sass|scss)$/,
            include: path.resolve(__dirname, 'src/scss'),
            use: [
                MiniCssExtractPlugin.loader,
                {
                    loader: 'css-loader',
                    options: {
                        url: (url, resourcePath) => {
                            return (/^\.\.\//.test(url));
                        }
                    }
                },
                {
                    loader: 'postcss-loader',
                    options: {
                        plugins: [
                            autoprefixer({})
                        ],
                        sourceMap: true
                    }
                },
                'sass-loader',
            ],
        },
        {
            test: /\.(eot|woff|woff2|ttf|svg|png|jpg|gif|cur)$/, use: [
                {
                    loader: 'file-loader',
                    options: {
                        outputPath: (url, resourcePath, context) => {
                            if (/(\\img\\tmp\\)|(\/img\/tmp\/)/.test(resourcePath)) {
                                return `/prod/img/tmp/${url}`;
                            }

                            if (/(\\img\\)|(\/img\/)/.test(resourcePath)) {
                                return `/prod/img/${url}`;
                            }

                            return `/prod/fonts/${url}`;
                        },
                        publicPath: (url, resourcePath, context) => {
                            if (/(\\img\\tmp\\)|(\/img\/tmp\/)/.test(resourcePath)) {
                                return `img/tmp/${url}`;
                            }

                            if (/(\\img\\)|(\/img\/)/.test(resourcePath)) {
                                return `img/${url}`;
                            }

                            return `../fonts/${url}`;
                        },
                        name: '[name].[ext]'
                    },
                },
            ],
        }, {
            test: /\.html$/,
            include: path.resolve(__dirname, 'src/template'),
            use: ['html-loader']
        },
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: './prod/css/style.bundle.css',
        }),
        new CopyWebpackPlugin([{
            from: './src/fonts',
            to: './prod/fonts'
        },
        {
            from: './src/favicon',
            to: './prod/favicon'
        },
        {
            from: './src/img',
            to: './prod/img'
        },
        ]),
    ].concat(htmlPlugins)
};