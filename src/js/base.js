global.jQuery = require('jquery');
global.$ = global.jQuery;

function preventDefaults (e) {
    e.preventDefault()
    e.stopPropagation()
}