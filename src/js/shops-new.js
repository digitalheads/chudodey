$(function () {
    if ($('#map').length) {
        ymaps.ready(function () {
            var map = new ymaps.Map('map', {
                center: [43.10091083, 131.90651923],
                zoom: 13
            });
            map.behaviors.disable('scrollZoom');

            objectManager = new ymaps.ObjectManager({
                // Чтобы метки начали кластеризоваться, выставляем опцию.
                clusterize: true,
                // ObjectManager принимает те же опции, что и кластеризатор.
                gridSize: 32,
                clusterDisableClickZoom: true
            });

            objectManager.objects.options.set('preset', 'islands#violetIcon');
            objectManager.clusters.options.set('preset', 'islands#violetClusterIcons');
            map.geoObjects.add(objectManager);

            objectManager.add(mapObjects);
        });
    }
});