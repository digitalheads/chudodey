$(function () {
    if ($('#sevenPFile').length) {
        let dropArea = document.getElementById('sevenPFile');

        ['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
            dropArea.addEventListener(eventName, preventDefaults, false)
        });

        document.getElementById('sevenPFilesInput').addEventListener('change', function (e) {
            handleFiles(this.files)
        }, false);

        dropArea.addEventListener('drop', function (e) {
            e.preventDefault()
            e.stopPropagation()

            let dt = e.dataTransfer

            let files = dt.files
            handleFiles(files)
        }, false)
    }
});

function preventDefaults(e) {
    e.preventDefault()
    e.stopPropagation()
}

function handleFiles(files) {
    files = [...files]
    files.forEach(uploadFile)
    files.forEach(previewFile)
}

function uploadFile(file) {
    var url = 'ВАШ URL ДЛЯ ЗАГРУЗКИ ФАЙЛОВ'
    var xhr = new XMLHttpRequest()
    var formData = new FormData()
    xhr.open('POST', url, true)
    xhr.addEventListener('readystatechange', function (e) {
        if (xhr.readyState == 4 && xhr.status == 200) {
            // Готово. Информируем пользователя
        }
        else if (xhr.readyState == 4 && xhr.status != 200) {
            // Ошибка. Информируем пользователя
        }
    })
    formData.append('file', file)
    xhr.send(formData)
}

function previewFile(file) {
    let reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onloadend = function () {
        $('#previewFiles').append($('<div>', {
            class: 'd-flex mb-2 align-items-center',
            append: $('<img>', { src: 'img/file_upload.svg', height: 15, class: 'mr-2' })
                .add('<span>', { text: file.name, class: 'mr-2 fs-16' })
                .add('<img>', { src: 'img/cart.svg', height: 15, class: 'btn btn-primary' }) // TODO CHANGE ICON AND ADD DELETE FUNCTION
        }
        ));
    }
}