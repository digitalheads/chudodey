/* */

import 'bootstrap/dist/js/bootstrap.bundle.min';


require('./base.js');
require('ion-rangeslider');
require('overlayscrollbars');
require('./shops-new.js');
require('./7prichin.js');
require('star-rating-svg2/dist/jquery.star-rating-svg.min');
require('slick-carousel');
require('./jquery.booklet.latest');
require('./jquery.easing.1.3');
const Slideout = require('slideout');

//search bar
$(function () {
    $.expr[":"].acp = function (elem, index, m) {
        if (typeof elem.className !== 'string') {
            return [];
        }

        var regString = '\\b' + m[3];
        var reg = new RegExp(regString, "g");
        return elem.className.match(reg);
    }

    let hamburger;
    var mobileHeader = document.querySelector('.mobile-header');

    $('.js-mobile-catalog').on('click', function () {
        hamburger = $(this).find('.hamburger');
        hamburger.toggleClass('is-active');

        slideout.toggle();
    });

    $('.menu-profile').on('click', function () {
        hamburger = $('.js-mobile-catalog').find('.hamburger');
        hamburger.toggleClass('is-active');

        slideoutProfile.toggle();
    });

    var slideout = new Slideout({
        'panel': document.getElementById('content'),
        'menu': document.getElementById('mobile-menu'),
        'padding': 256,
        'tolerance': 70
    });

    var slideoutProfile = new Slideout({
        'panel': document.getElementById('content'),
        'menu': document.getElementById('mobile-profile-menu'),
        'padding': 256,
        'tolerance': 70,
        'side': 'right',
    });

    slideout.on('translate', function (translated) {
        mobileHeader.style.transform = 'translateX(' + translated + 'px)';
    });
    slideoutProfile.on('translate', function (translated) {
        mobileHeader.style.transform = 'translateX(' + translated + 'px)';
    });

    slideout.on('beforeopen', function () {
        mobileHeader.style.transition = 'transform 300ms ease';
        mobileHeader.style.transform = 'translateX(256px)';
        $('#mobile-menu').show();
        $('#mobile-profile-menu').hide();
    });

    slideoutProfile.on('beforeopen', function () {
        mobileHeader.style.transition = 'transform 300ms ease';
        mobileHeader.style.transform = 'translateX(-256px)';
        $('#mobile-menu').hide();
        $('#mobile-profile-menu').show();
    });

    slideout.on('beforeclose', function () {
        this.panel.removeEventListener('click', closeSlideout);
        $('.mobile-header')[0].removeEventListener('click', closeSlideout);
        hamburger.removeClass('is-active');
        mobileHeader.style.transition = 'transform 300ms ease';
        mobileHeader.style.transform = 'translateX(0px)';
    });

    slideoutProfile.on('beforeclose', function () {
        this.panel.removeEventListener('click', closeSlideout);
        $('.mobile-header')[0].removeEventListener('click', closeSlideout);
        hamburger.removeClass('is-active');
        mobileHeader.style.transition = 'transform 300ms ease';
        mobileHeader.style.transform = 'translateX(0px)';
    });

    slideout.on('open', function () {
        $('.mobile-header')[0].addEventListener('click', closeSlideout);
        this.panel.addEventListener('click', closeSlideout);
        mobileHeader.style.transition = '';
    });

    slideoutProfile.on('open', function () {
        $('.mobile-header')[0].addEventListener('click', closeSlideout);
        this.panel.addEventListener('click', closeSlideout);
        mobileHeader.style.transition = '';
    });

    slideout.on('close', function () {
        mobileHeader.style.transition = '';
    });

    slideoutProfile.on('close', function () {
        mobileHeader.style.transition = '';
    });

    let closeSlideout = function (evt) {
        evt.preventDefault();
        slideout.close();
        slideoutProfile.close();
    };

    slideout.disableTouch();
    slideoutProfile.disableTouch();

    if ($('#journalPages').length) {
        $('#journalPages').booklet({
            hovers: true,
            overlay: true,
            pagePadding: 0,
            width: '100%',
            height: '100%',
            arrows: true,
            pageNumbers: false,
            change: function (event, data) {

                var maxHeight = 0;

                $('.journal__page').each(function () {
                    if ($(this).height() > maxHeight) {
                        maxHeight = $(this).height();
                    }
                });
                $('.b-wrap').height(maxHeight + 'px');
                $('.b-page').height(maxHeight + 'px');
                $('#journalPages').height(maxHeight + 'px');

                $('.b-counter.first').text(data.index + 1);
                $('.b-counter.second').text(data.index + 2);
            }
        });

        $(".journal__img").one("load", function () {
            // $('.journal>div>div').width(($(this).width() * 2) + 'px');

            var maxHeight = 0;

            $('.journal__page').each(function () {
                if ($(this).height() > maxHeight) {
                    maxHeight = $(this).height();
                }
            });

            $('.b-wrap').height(maxHeight + 'px');
            $('.b-page').height(maxHeight + 'px');
            $('#journalPages').height(maxHeight + 'px');
        }).each(function () {
            if (this.complete) {
                $(this).trigger('load');
            }
        });
    }

    $('.index-products').on('init', function (event, slick) {
        var dotdotInterval = setInterval(function () {
            if ($('h3.productdotdot').parent().parent().width() > 300) {
                return;
            }

            clearInterval(dotdotInterval)

            $('h3.productdotdot').each(function () {
                let h3ScrollHeight = 25;

                let hasColourPicker = $(this).closest('.product').children('.product__bottom').children('.product__colours').length;

                $(this).data('content', $(this).html());

                let wordArray = $(this).html().split(' ');
                let wasTransformed = false;

                while ($(this)[0].scrollHeight > (hasColourPicker ? h3ScrollHeight : h3ScrollHeight * 2)) {

                    wasTransformed = true;

                    if ($(this).html() === '...') {
                        wordArray = $(this).data('content').split(' ');
                    }

                    wordArray.pop();
                    $(this).html(wordArray.join(' ') + '...');
                }

                $(this).data('contentSmall', $(this).html());

                var self = this;

                if (wasTransformed) {
                    $(this).closest('.product').hover(
                        function () {
                            $(self).html($(self).data('content'));
                            $(self).addClass('mh-100');
                        },
                        function () {
                            $(self).html($(self).data('contentSmall'));
                            $(self).removeClass('mh-100');
                        }
                    );
                } else {

                    $(this).closest('.product').hover(
                        function () {
                            $(self).css('margin-bottom', '1.3em');
                        },
                        function () {
                            $(self).css('margin-bottom', '1.3em');
                        }
                    );
                }
            });
        }, 5);

    });

    if ($('#index-banner').length) {
        $('#index-banner').slick({
            arrows: true,
            dots: true,
            autoplay: true,
            autoplaySpeed: 5000,
            adaptiveHeight: true,
            prevArrow: '<button type="button" class="slick-prev slick-prev_index-banner">Previous</button>',
            nextArrow: '<button type="button" class="slick-next slick-next_index-banner">Next</button>',
            respondTo: 'window',
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        prevArrow: '<button type="button" class="slick-prev slick-prev_before_none_left"><img src="img/left-arrow.png"></button>',
                        nextArrow: '<button type="button" class="slick-next slick-prev_before_none_right"><img src="img/right-arrow.png"></button>',
                    }
                }
            ]
        });
    }

    $('.index-products').each(function () {
        let productCount = 4;
        let arrows = true;

        if ($(this).data('productCount')) {
            productCount = $(this).data('productCount');
        }

        if (typeof $(this).data('slickArrows') !== 'undefined') {
            arrows = $(this).data('slickArrows') === 'true';
        }

        $(this).slick({
            arrows: arrows,
            dots: false,
            slidesToScroll: productCount,
            slidesToShow: productCount,
            adaptiveHeight: true,
            swipeToSlide: true,
            prevArrow: '<button type="button" class="slick-prev slick-prev_index-banner">Previous</button>',
            nextArrow: '<button type="button" class="slick-next slick-next_index-banner">Next</button>',
            respondTo: 'window',
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        arrows: false,
                        dots: true,
                        prevArrow: '<button type="button" class="slick-prev slick-prev_before_none_left"><img src="img/left-arrow.png"></button>',
                        nextArrow: '<button type="button" class="slick-next slick-prev_before_none_right"><img src="img/right-arrow.png"></button>',
                    }
                },
            ]
        });
    });

    if ($('#journalMobile').length) {
        $('#journalMobile').slick({
            centerMode: true,
            centerPadding: '30px',
            arrows: false,
            dots: false,
            slidesToScroll: 1,
            slidesToShow: 1,
            adaptiveHeight: true,
            swipeToSlide: true,
        });
    }

    if ($('#index-makeup-lessons').length) {
        $('#index-makeup-lessons').slick({
            arrows: true,
            dots: false,
            slidesToScroll: 3,
            slidesToShow: 3,
            prevArrow: '<button type="button" class="slick-prev slick-prev_before_none_left"><img src="img/left-arrow.png"></button>',
            nextArrow: '<button type="button" class="slick-next slick-prev_before_none_right"><img src="img/right-arrow.png"></button>',
            respondTo: 'window',
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                },
            ]
        });
    }

    if ($('.promo__slick').length) {
        $('.promo__slick').slick({
            arrows: true,
            dots: false,
            slidesToScroll: 2,
            slidesToShow: 2,
            prevArrow: '<button type="button" class="slick-prev slick-prev_index-banner slick-prev_promo">Previous</button>',
            nextArrow: '<button type="button" class="slick-next slick-next_index-banner slick-next_promo">Next</button>',
            respondTo: 'window',
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                },
            ]
        });
    }

    if ($(window).width() < 576) {
        $('.advantages>div').slick({
            arrows: true,
            dots: false,
            slidesToScroll: 1,
            slidesToShow: 1,
            prevArrow: '<button type="button" class="slick-prev slick-prev_index-banner slick-prev_promo">Previous</button>',
            nextArrow: '<button type="button" class="slick-next slick-next_index-banner slick-next_promo">Next</button>',
            respondTo: 'window',
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        prevArrow: '<button type="button" class="slick-prev slick-prev_before_none_left"><img src="img/left-arrow.png"></button>',
                        nextArrow: '<button type="button" class="slick-next slick-prev_before_none_right"><img src="img/right-arrow.png"></button>',
                    }
                },
            ]
        });
    }

    $('.choose-city-btn').on('click', function () {
        $('#chooseCityModal').modal('show');
    });

    $('.choose-city-full-btn').on('click', function () {
        $('#chooseCityFullModal').modal('show');
    });

    // $('.product').on('click', function () {
    //     Math.floor(new Date().getTime() / 1000) % 2 ? $('#productModal').modal('show') : $('#productSecondModal').modal('show');
    // });


    var test = function () {

        $('#openMap').on('click', function () {
            $('#siteMap').toggle();
        });
        $('.mobile-logo-image').on('click', function () {
            $('#siteMap').toggle();
        });

        $('#testOpenMessageModal').on('click', function () {
            $('#messageSendedModal').modal('show');
        });

        $('#testOpenResumeModal').on('click', function () {
            $('#resumeSendedModal').modal('show');
        });

        $('#testOpenReviewSendedModal').on('click', function () {
            $('#reviewSendedModal').modal('show');
        });

        $('#testOpencityChoose1Modal').on('click', function () {
            $('#chooseCityModal').modal('show');
        });

        $('#testOpenCityChoose2Modal').on('click', function () {
            $('#chooseCityFullModal').modal('show');
        });

        $('#testOpenProduct1Modal').on('click', function () {
            $('#productModal').modal('show');
        });

        $('#testOpenProduct2Modal').on('click', function () {
            $('#productSecondModal').modal('show');
        });
        $('#testAuthModal').on('click', function () {
            $('#authModal').modal('show');
        });
        $('#authModal .btn').on('click', function () {
            $('#authModal').toggleClass('error');
        });
        $('#testOrderModal').on('click', function () {
            $('#orderModal').modal('show');
        });
        // $('.switcher-forgon-password').on('change', function () {
        //     let jqSelf = $(this);
        //     if (jqSelf.is(':checked')) {
        //         $('.switched-forgon-password').hide();
        //         $('.switched-forgon-password.' + jqSelf.attr('rel') + '-forgon-password').show();
        //     }
        // });
        $(':acp("switcher-")').on('change', function () {
            let jqSelf = $(this);

            let classId = this.className.match(/switcher-(.+)/g);

            if (classId.length > 0) {
                for (let i = 0; i < classId.length; i++) {
                    let id = classId[i].split('-')[1];

                    if (jqSelf.is(':checked')) {
                        $('.switched-' + id).hide();
                        $('.switched-' + id + '.' + jqSelf.attr('rel') + '-switcher').show();
                    }
                }
            }
        });
    };
    test();

    $('.js-global-menu-button').on('click', function () {
        let globalMenuMobile = $('.js-global-menu');
        globalMenuMobile.toggleClass('active');
    });

    $('.js-params-toggle').on('click', function () {
        let paramsBlock = $('.js-params-block');
        paramsBlock.toggleClass('active');
    });

    let searchToggleButton = $('#searchToggleButton');
    let buttonHideSearchContainer = $('#buttonHideSearchContainer');
    let searchContainer = $('#searchContainer');
    let overlay = $('#backgroundOverlay');
    let searchToggleButtonSpan = $('#searchToggleButton>span');

    let rangeSlider = $('.js-range-slider');
    let overlayScrollBars = $('.js-scrollable');
    let starRatingReadOnly = $('.js-rating-read-only');
    let starRating = $('.js-rating');

    //range slider init
    rangeSlider.ionRangeSlider();

    //overlayscrollbars init
    overlayScrollBars.overlayScrollbars({});

    //star-rating-svg
    starRatingReadOnly.starRating({
        starSize: 18,
        useGradient: false,
        activeColor: '#fccd2b',
        hoverColor: '#fccd2b',
        ratedColor: '#fccd2b',
        strokeColor: 'transparent',
        emptyColor: '#cacaca',
        readOnly: true
    });

    starRating.starRating({
        starSize: 25,
        initialRating: 3,
        useGradient: false,
        activeColor: '#fccd2b',
        hoverColor: '#fccd2b',
        ratedColor: '#fccd2b',
        strokeColor: 'transparent',
        emptyColor: '#cacaca',
    });

    //search bar
    document.onclick = function (e) {
        if (e.target === overlay[0]) {
            searchContainer.slideUp(100);
            overlay.hide();
        }
        if (e.target === searchToggleButton[0] || e.target === searchToggleButtonSpan[0]) {
            searchContainer.slideToggle(100);
            overlay.toggle();
        }
        if (e.target === buttonHideSearchContainer[0]) {
            searchContainer.slideUp(100);
            overlay.hide();
        }
    };

    //Dropdown menu
    let dropDownToggle = $('.dropdown-toggle');
    let dropDownMenu = $('.dropdown-menu');

    dropDownToggle.hover(
        function () {
            $(this).data('mouse-hover', true);

            let dropDownElement = $(this).parent().find('.dropdown-menu');

            if (typeof dropDownElement !== 'undefined') {
                dropDownElement.show();
            }

        },
        function () {
            $(this).data('mouse-hover', false);

            let dropDownElement = $(this).parent().find('.dropdown-menu');

            setTimeout(function () {
                if (typeof dropDownElement !== 'undefined' && !dropDownElement.data('mouse-hover')) {
                    dropDownElement.hide();
                }
            }, 30);

        }
    );

    dropDownMenu.hover(
        function () {
            $(this).data('mouse-hover', true);

            let dropDownToggle = $(this).parent().find('.dropdown-toggle');

            if (typeof dropDownToggle !== 'undefined') {
                dropDownToggle.addClass('nav-link_hover');
            }
        },
        function () {
            $(this).data('mouse-hover', false);

            let dropDownToggle = $(this).parent().find('.dropdown-toggle');

            var dropDownElement = $(this);

            if (typeof dropDownToggle !== 'undefined') {
                dropDownToggle.removeClass('nav-link_hover');
            }

            setTimeout(function () {
                if (typeof dropDownToggle !== 'undefined' && !dropDownToggle.data('mouse-hover')) {
                    dropDownElement.hide();
                }
            }, 30);
        }
    );

    $('.photo-picker__item').on('click', function () {
        $('#' + $(this).data('mainImage')).attr("src", $(this).children().attr('src'));
    });

    $('.product__colours').find('.colour-select').each(function () {
        $(this).on('click', function () {
            $(this).parent().find('.colour-select').each(function () {
                $(this).removeClass('colour-select_selected');
            });
            $(this).addClass('colour-select_selected');
        });
    });

    $('.sel').each(function () {
        $(this).children('select').css('display', 'none');

        var $current = $(this);

        $(this).find('option').each(function (i) {
            if (i == 0) {
                $current.prepend($('<div>', {
                    class: $current.attr('class').replace(/sel/g, 'sel__box')
                }));

                var placeholder = $(this).text();
                $current.prepend($('<span>', {
                    class: $current.attr('class').replace(/sel/g, 'sel__placeholder'),
                    text: placeholder,
                    'data-placeholder': placeholder
                }));

                $(this).addClass('selected');
            }

            $current.children('div').append($('<span>', {
                class: $current.attr('class').replace(/sel/g, 'sel__box__options'),
                text: $(this).text()
            }));
        });
    });

    // Toggling the `.active` state on the `.sel`.
    $('.sel').click(function () {
        $(this).toggleClass('active');
    });

    // Toggling the `.selected` state on the options.
    $('.sel__box__options').click(function () {
        var txt = $(this).text();
        var index = $(this).index();

        $(this).siblings('.sel__box__options').removeClass('selected');
        $(this).addClass('selected');

        var $currentSel = $(this).closest('.sel');
        $currentSel.children('.sel__placeholder').text(txt);
        $currentSel.children('select').prop('selectedIndex', index + 1);
    });

    $('#searchMobileContainer').on('click', function () {
        $('#searchContainerMobile').slideToggle();
    });

    $('.product .product__bottom .product__buy').on('click', function () {
        let product = $(this).closest('.product');

        if (product.children('.product__cart').length) {
            product.children('.product__cart').show();
            $(this).closest('.product__bottom').hide();
        }
    });

    $.fn.inputFilter = function (inputFilter) {
        return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function () {
            if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            }
        });
    };

    $('.input-counter').each(function () {
        var self = this;
        $(this).find('input[type="text"]').inputFilter(function (value) {
            return /^\d*$/.test(value);
        });

        $(this).find('.input-group-prepend>button').on('click', function () {
            if (parseInt($(self).find('input[type="text"]').val()) > 0) {
                if ($(this).parents('.certificat__card').length && parseInt($(self).find('input[type="text"]').val()) - 1 === 0) {
                    $(self).find('.input-group-append>button').css('background-color', '');
                    $(self).find('.input-group-append>button').css('border-color', '');
                }
                $(self).find('input[type="text"]').val(parseInt($(self).find('input[type="text"]').val()) - 1);
                $(self).find('input[type="text"]').trigger('change');
            }
        });

        $(this).find('.input-group-append>button').on('click', function () {
            if ($(this).parents('.certificat__card').length) {
                $(this).css('background-color', '#17a2b8');
                $(this).css('border-color', '#17a2b8');
            }
            $(self).find('input[type="text"]').val(parseInt($(self).find('input[type="text"]').val()) + 1);
            $(self).find('input[type="text"]').trigger('change');
        });
    });

    $('.password-container>.password-container__icon').on('click', function () {
        let jqIcon = $(this);
        let jqInput = $(this).parent().find('input');

        if (jqInput.attr('type') === 'password') {
            jqIcon.addClass('show');
            jqInput.attr('type', 'text');
        } else {
            jqIcon.removeClass('show');
            jqInput.attr('type', 'password');
        }
    });


    $('[data-toggle="tooltip"]').tooltip()

    let cartCollapsedBtn = $('.collapse-btn');
    let cartCollapse = $('.table-cart_additions')
    if (cartCollapsedBtn.length > 0 && cartCollapse.length > 0) {
        let cartCollapsedBtnText = cartCollapsedBtn.html();
        let cartCollapsedBtnTextClosed = cartCollapsedBtn.data('closed');
        let cartCollapseAddition = $('.cart-collapse-addition');

        cartCollapse.on('shown.bs.collapse', function() {
            cartCollapsedBtn.html(cartCollapsedBtnTextClosed);
            cartCollapseAddition.hide();
        });
        
        cartCollapse.on('hidden.bs.collapse', function() {
            cartCollapsedBtn.html(cartCollapsedBtnText);
            cartCollapseAddition.show();
        });

        cartCollapsedBtn.on('click', function() {
            cartCollapse.collapse('toggle');
        });
    }
});
