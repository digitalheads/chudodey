# chudodey
Веб-сайт для Чудодея

```
sass/ 
    -base/
        -animations/  # анимации
        -typography/  # типография
        -variables/   # переменные
        -mixins/      # миксины
        -functions/   # функции
        -utilities/   # утилиты
    -components/      # отдельные компоненты
    -pages/           # страницы
```

Для сборки необходим [Node](https://nodejs.org/en/)

## Разворачивание на проде

```
> npm install
> npm run prod
```

## Команды

```
npm run dev - Собрать версию для разработки (Сохраняется в dist/dev)
npm run build - Собрать версию для прода (Сохраняется в dist/prod), Отличие от дева - скрипты и стили минифицируются
npm run watch - Автоматическая сборка дева при изменении фалов скриптов/стилей/страниц
```